import numpy as np
from typing import List

def get_average(numbers: List[float]) -> float:
    return np.mean(numbers)